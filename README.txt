INSTRUCTIONS
-------------
Download the module.
Download the phpvideotoolkit library (https://github.com/buggedcom/phpvideotoolkit-v2/releases).
Place it under sites/all/libraries/phpvideotoolkit so that the file can be found under sites/all/libraries/phpvideotoolkit/autoloader.php.
Enable the module.


This module does nothing by itself, it just provides the phpvideotoolkit library so that other modules can use it.

You can load the library like this:
<?php
libraries_load('phpvideotoolkit');
?>
